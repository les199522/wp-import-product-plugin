<?php
/**
 * Plugin Name: TS Import product
 * Plugin URI:
 * Description:  Import product from CSV file.
 * Version: 1.0
 * Author: Cimpleo
 * Author URI: http://cimpleo.com
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 *
 * @package csv-import
 * @author Cimpleo
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
final class ts_import {

	/**
	 * WooCommerce version.
	 *
	 * @var string
	 */
	public $version = '1.0';

	/**
	 * [__construct description]
	 */
	public function __construct() {
		$this->init_hooks();
		$this->includes();
		$this->define_constants();

	}

	/**
	 * Hook into actions and filters.
	 * @since  2.3
	 */
	private function init_hooks() {
	// Plugin actions
		register_activation_hook( __FILE__, array( $this, 'installPlugin' ) );
		register_deactivation_hook( __FILE__,  array( $this, 'deactivatePlugin' ) );	
	// Actions
		add_action( 'admin_menu', array( $this, 'add_plugin_menu_page' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_styles_scripts' ) );

	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
	// Class
		include_once( $this->plugin_path().'/include/File_CSV_DataSource/DataSource.php' );
		include_once( $this->plugin_path().'/include/class_productImport.php' );
		include_once( $this->plugin_path().'/include/class-tsImport.php' );
		include_once( $this->plugin_path().'/include/class-tsPricingTool.php' );
		include_once( $this->plugin_path().'/include/class-ajaxRequests.php' );
	}

	/**
	 * [installPlugin description]
	 * 
	 */
	public static function installPlugin() {
		if ( ! current_user_can( 'activate_plugins' ) )
			return;

	// Create Folder
		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/ts-import/';
		wp_mkdir_p( $upload_dir );
	// Add default settings
		update_option( 'CJAffiliate_plugin_imported_ids', array() );

	}

	/**
	 * [deactivatePlugin description]
	 * 
	 */
	public static function deactivatePlugin() {
		if ( ! current_user_can( 'activate_plugins' ) )
			return;

		delete_option( 'CJAffiliate_plugin_imported_ids' );
	}

	/**
	 * [define_constants description]
	 * 
	 */
	private function define_constants() {
		$upload_dir = wp_upload_dir();

		$this->define( 'CJAFFILIATE_VERSION', $this->version );
		$this->define( 'TSIMPORT_DIR', $upload_dir['basedir'] . '/ts-import/' );
		$this->define( 'TSIMPORT_DIRURL', $upload_dir['baseurl'] . '/ts-import/' );
	}


	/**
	 * [admin_styles_scripts description]
	 * 
	 */
	public function admin_styles_scripts() {
	// Custom style
		wp_enqueue_style( 'csvImport-style.css', $this->plugin_url('/assets/main.css') );
		wp_enqueue_style( 'csvimport-select2.css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css' );
	// Script
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery.form', $this->plugin_url('/assets/jquery.form.min.js'), array( 'jquery' ) );
		wp_enqueue_script( 'csvImport-main.js', $this->plugin_url('/assets/main.js'), array( 'jquery.form' ) );
		wp_enqueue_script( 'csvImport-select2.js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js');
	}

	/**
	 * [add_plugin_menu_page description]
	 */
	public function add_plugin_menu_page() {
		add_submenu_page( 'tools.php', 'CSV Import Product', 'CSV Import', 'manage_options', 'csvimport',  array( $this, 'render_page_import' ) );
	}

	/**
	 * [render_page_settings_import description]
	 * @return [type] [description]
	 */
	public function render_page_import() {
		include_once( $this->plugin_path().'/include/page-import.php' );
	}





	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Get the plugin url.
	 * @return string
	 */
	public function plugin_url( $path ) {
		return untrailingslashit( plugins_url( $path, __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
	

}
new ts_import();	
