jQuery(document).ready(function($) {
	var pricingLoader = $('.pricing-tool .cj-loader');
	$('.select2').select2();
	$('.js-choose-artist').on('change', function(event) {
		if ( $(this).val() != '' ) {
			$('.js-getchooseartist').data('artist', $(this).val()).prop('disabled', false);
			$('.js-getchooseartist').text('Get ' + $('.js-choose-artist option:selected').text());
		}
	});

	$('.js-pricesimport-form').on('submit', function(event) {
		event.preventDefault();
		/* Act on the event */
	    var options = { 
	        beforeSubmit:  function(){
	        	pricingLoader.show();
	        },
	        success: function(data){
	        	pricingLoader.hide();
	        	// console.log(data);
	        },  
	        // other available options: 
	        url:       $(this).attr('action'),         
	        type:      'POST',        
	        clearForm: true,
	        data: {
	        	action: 'ts_pricesImport_action'
	        }

	    }; 
	    // bind form using 'ajaxForm' 
	    $(this).ajaxSubmit(options); 
	});

	$('.js-getchooseartist').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var atrist_id = $(this).data('artist');
		pricingLoader.show();
		$('.download-link').removeClass('open');
		$('.pricing-tool .notice').html('');
		$.ajax({
			url: $('.js-csvImport').attr('action'),
			type: 'POST',
			data: {
				action: 'generate_chooseArtist_PricingDoc',
				_nonce: $('.pricing-tool #_wpnonce').val(),
				artist: atrist_id
			},
		})
		.success(function(data) {
			$('.pricing-tool .cj-loader').hide();
			// console.log(data);
			if ( data.success === true ) {
				$('.download-link').attr('href', data.data).addClass('open');
			}else {
				$('.pricing-tool .notice').html('We did not find tickets of this artist without the established prices');
			}
		})
		.fail(function() {
			console.log("error");
		})
	});

	$('.js-getwithoutprices').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		pricingLoader.show();
		$('.download-link').removeClass('open');
		$('.pricing-tool .notice').html('');

		$.ajax({
			url: $('.js-csvImport').attr('action'),
			type: 'POST',
			data: {
				action: 'generate_withoutPrice_PricingDoc',
				_nonce: $('.pricing-tool #_wpnonce').val()
			},
		})
		.success(function(data) {
			// console.log(data);
			pricingLoader.hide();
			if ( data.success === true ) {
				$('.download-link').attr('href', data.data).addClass('open');
			}
		})
		.fail(function() {
			console.log("error");
		})
	});

	$('.js-getlastimport').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		pricingLoader.show();
		$('.download-link').removeClass('open');
		$('.pricing-tool .notice').html('');

		$.ajax({
			url: $('.js-csvImport').attr('action'),
			type: 'POST',
			data: {
				action: 'generate_lastImport_PricingDoc',
				_nonce: $('.pricing-tool #_wpnonce').val()
			},
		})
		.success(function(data) {
			// console.log(data);
			pricingLoader.hide();
			if ( data.success === true ) {
				$('.download-link').attr('href', data.data).addClass('open');
			}else {
				$('.pricing-tool .notice').html(data.data);
			}
		})
		.fail(function() {
			console.log("error");
		})



	});

	$('.js-rollback-btn').on('click', function(event) {
		event.preventDefault();
		$.ajax({
			url:  $('.js-csvImport').attr('action'),
			type: 'POST',
			data: {
				action: 'plugin_csv_rollback_import'
			},
			beforeSend: function(){
				$('.cj-spinner').show();
			}

		})
		.success(function(data) {
			$('.cj-spinner').hide();
			if ( data.success == 'true' ) {
				$('.js-rollback-btn').addClass('btn-success');
			}else {
				$('.js-rollback-btn').addClass('btn-error');
				$('.js-rollbackComplete').html( data.data ).show("slow");
			}
		})
		.fail(function() {
			$('.js-rollback-btn').addClass('btn-warning');
		})
	});

	$('.js-csvImport').on('submit', function(event) {
		event.preventDefault();
		/* Act on the event */
	    var options = { 
	        beforeSubmit:  function(){
	        	$('.js-csvImport').find('.btn-submit').removeClass('btn-success');
				$('.js-csvImport').find('.btn-submit').removeClass('btn-warning');
				$('.js-rollbackComplete').hide();
				$('.js-rollback-btn').hide();
				$('.cj-spinner').show();
	        },
	        success: function(data){
	        	$('.cj-spinner').hide();

	        	if ( data.success == false ) {
	        		$('.js-csvImport').find('.btn-submit').addClass('btn-warning');
	        		$('.js-rollbackComplete').html( data.data ).show();
	        	}else {
	        		$('.js-csvImport').find('.btn-submit').addClass('btn-success');
	        		$('.js-rollback-btn').show();
	        	}

	        },  
	        // other available options: 
	        url:       $(this).attr('action'),         
	        type:      'POST',        
	        clearForm: true,
	        data: {
	        	action: 'plugin_csv_import_file'
	        }

	    }; 
	    // bind form using 'ajaxForm' 
	    $(this).ajaxSubmit(options); 
	});

});