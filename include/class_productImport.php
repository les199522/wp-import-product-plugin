<?php 
/**
 * Class action import custom products for Ticket-selection 
 *
 * @author Vlad lesovskiy ( Cimpleo )
 * 
 */

class productImport {

	public $attrName;

	/**
	 * [__construct description]
	 */
	function __construct() {
		$this->init_hooks();
	}


	/**
	 * [init_hooks description]
	 * 
	 */
	private function init_hooks(){
		// Ajax actions
		add_action( 'wp_ajax_plugin_csv_import_file', array( $this, 'ajax_importRequest' ) );
		add_action( 'wp_ajax_plugin_csv_rollback_import', array( $this, 'ajax_rollbackImport' ) );

	}

	/**
	 * [ajax_rollbackImport description]
	 * @return [type] [description]
	 */
	public function ajax_rollbackImport() {
		$rollback_ids = get_option( 'CJAffiliate_plugin_imported_ids' );
		if ( !empty( $rollback_ids ) ) {
			foreach ($rollback_ids as $key => $id) {
				wp_delete_post( $id, true );
			}
			update_option( 'CJAffiliate_plugin_imported_ids', '' );
			wp_send_json_success( 'Rollback complete!' );
		} else {
			wp_send_json_error( 'Nothing to rollback' );
		}
	}


	/**
	 * [ajax_importRequest description]
	 * @return [type] [description]
	 */
	public function ajax_importRequest() {
	// Check all 
		check_ajax_referer( 'importCsv_plugin', 'nonce' ); 
		if ( empty( $_FILES ) )
			wp_send_json_error( 'File not found!' );

		include_once( 'File_CSV_DataSource/DataSource.php' );

		$csv = new File_CSV_DataSource;
		$file = $_FILES['importFile']['tmp_name'];

		$this->stripBOM( $file );

		if ( !$csv->load( $file ) )
			wp_send_json_error('Failed to load file, aborting.');

		// pad shorter rows with empty values
		$csv->symmetrize();

	// Specila options for Bulk import in DB.
		global $wpdb;
		wp_defer_term_counting( true );
		wp_defer_comment_counting( true );
		$wpdb->query( 'SET autocommit = 0;' );

			$this->importProduct_action( $csv );
	// Reset options
		wp_defer_term_counting( false );
		wp_defer_comment_counting( false );
		$wpdb->query( 'COMMIT;' );
		$wpdb->query( 'SET autocommit = 1;' );

		if (file_exists($file)) {
			@unlink($file);
			wp_send_json_success();
		}

	}


	/**
	 * [importProduct_action description]
	 * @param  [type] $csv [description]
	 * @return [type]      [description]
	 */
	public function importProduct_action( $csv ){
		if ( !isset( $csv ) )
			return false;

		$this->attrName = wc_attribute_taxonomy_name( 'Seat location' );

		$time_start = microtime(true);

		$parent_id = '';
		$imported_ids = array();
		foreach ($csv->connect() as $item_key => $item_value) {
			if ( empty( $item_value['Price'] ) ) {
				// if ( !empty( $parent_id ) ) 
				// 	WC_Product_Variable::sync( $parent_id );
				
				$parent_id = $this->createProduct( $item_value );
				if ( $parent_id != false ) {
					$id = $parent_id;	
				}
			}else {
				$child_id = $this->createProductVariation( $item_value, $parent_id );
				$id = $child_id;	

			}	

			$imported_ids[] = $id;
		}
		update_option( 'CJAffiliate_plugin_imported_ids', $imported_ids );
	//Debug
		$time_log = 'Imported all posts for = ' . (microtime(true) - $time_start);
		$this->log( $time_log );
	}



	/**
	 * [createProduct description]
	 * @param  [type] $product_item [description]
	 * @return [type]               [description]
	 */
	public function createProduct( $product_item ) {
		$post_data = array(
			'post_type'		=> 'product',
			'post_title'    =>  wp_strip_all_tags( $product_item['Title'] ),
			'post_content'  =>  $product_item['Description'],
			'post_status'   =>  'pending',
		);

		$product_parent_id = wp_insert_post( $post_data );

		if ( $product_parent_id ) {
		// Set event type
			if ( !empty( $product_item['Event type'] ) ) {
				$this->set_product_type( $product_item['Event type'], $product_parent_id );
			}
		// Set Artists
			if ( !empty( $product_item['Artist'] ) ) {
				wp_set_object_terms( $product_parent_id, $product_item['Artist'], 'artists' );
			}
		// Set stock status 
			update_post_meta( $product_parent_id, '_stock_status', 'instock' );
		// Set product type as variable !!!! 
			wp_set_object_terms( $product_parent_id, 'simple', 'product_type', false );
		// Add attr in product 
			$attributes = array(
				$this->attrName=> array(
					'name' => $this->attrName,
					'value' =>'',
					'is_visible' => '1',
					'is_variation' => '1',
					'is_taxonomy' => '1'
					)
				);
			update_post_meta( $product_parent_id, '_product_attributes', $attributes );

			if ( !empty( $product_item['Seat location'] ) ) {
				$attr_term = explode( '|', $product_item['Seat location'] );
				wp_set_object_terms( $product_parent_id, $attr_term, $this->attrName );
			}
		// Set meta value 
			if ( !empty( $product_item['Meta data'] ) ) {
				$meta_value = explode( '|', $product_item['Meta data'] );
				if ( is_array( $meta_value ) ) {
					update_post_meta( $product_parent_id, 'City', $meta_value[0] );
				// Bad hook...
					update_post_meta( $product_parent_id, 'Date', $meta_value[1] );
					update_post_meta( $product_parent_id, '_date', $meta_value[1] );
					update_post_meta( $product_parent_id, 'Venue', $meta_value[2] );
				}
			}

		// Set SEO fields ( keywords )
			if ( !empty( $product_item['Keywords'] ) ) {
				update_post_meta( $product_parent_id, '_yoast_wpseo_focuskw_text_input', $product_item['Keywords'] );
				update_post_meta( $product_parent_id, '_yoast_wpseo_focuskw', $product_item['Keywords'] );
			}
			
		// Set SEO fields ( Meta title )
			if ( !empty( $product_item['Meta title'] ) )
				update_post_meta( $product_parent_id, '_yoast_wpseo_title', $product_item['Meta title'] );	

		// Set SEO fields ( Meta desc )
			if ( !empty( $product_item['Meta description'] ) )
				update_post_meta( $product_parent_id, '_yoast_wpseo_metadesc', $product_item['Meta description'] );

		// Attached images 
			if ( !empty( $product_item['Product Image'] ) )
				$this->attached_post_image( $product_item['Product Image'], $product_item['Alt'], $product_parent_id );
		// Featured image 
			if ( !empty( $product_item['Featured image2'] ) ) {
				$arrayFeatured = array();
				$default_string = home_url().'/wp-content/uploads';
				$featured_images_id = $this->get_attachment_id_by_filename( $product_item['Featured image2'] );


				if ( $featured_images_id ) {
					$full_featured_image = wp_get_attachment_image_src( $featured_images_id, 'full' );
					$thumbnail_featured_image = wp_get_attachment_image_src( $featured_images_id, 'thumbnail' );

					$arrayFeatured[0] = str_replace( $default_string, '', $thumbnail_featured_image[0] );
					$arrayFeatured[1] = str_replace( $default_string, '', $full_featured_image[0] );
					$stringFeatured = implode( ',', $arrayFeatured );
					if ( !empty( $arrayFeatured ) ) {
						$complete_array_featured[0] = implode( ',', $arrayFeatured );
						update_post_meta( $product_parent_id, 'dfiFeatured', $complete_array_featured );
					}
				}
			}

			return $product_parent_id;
		}
		$this->log( 'What is wrong with product import' . $product_itemp['Title'] );
		return false;
	}

	/**
	 * [createProductVariation description]
	 * @param  [type] $variationItem [description]
	 * @param  [type] $parent_id     [description]
	 * @return [type]                [description]
	 */
	private function createProductVariation( $variationItem, $parent_id ){
		if ( !isset( $parent_id ) )
			return false;
						
		$variation = array(
			'post_title'   => 'Product #' . $parent_id . ' Variation',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_parent'  => $parent_id,
			'post_type'    => 'product_variation'
			);
		// The variation id
		$variation_id = wp_insert_post( $variation );

		if ( $variation_id ) {
		// Regular Price ( you can set other data like sku and sale price here )
			update_post_meta( $variation_id, '_regular_price', $variationItem['Price'] );
			update_post_meta( $variation_id, '_price',  $variationItem['Price'] );

		// Assign the size and color of this variation
			// $attr = str_replace(' ', '-', strtolower( $variationItem['Seat location'] ) );
			$attr = sanitize_title( $variationItem['Seat location'] ); 
			update_post_meta( $variation_id, 'attribute_' . $this->attrName,  $attr );

		// Attach image 
			if ( !empty( $variationItem['Product Image'] ) ) {
				$this->attached_post_image( $variationItem['Product Image'], $variationItem['Alt'], $variation_id );
			}

			return $variation_id;
		}		
		return true;
	}

	/**
	 * [attached_post_image description]
	 * @param  [type] $image_name [description]
	 * @param  string $alt        [description]
	 * @param  [type] $product_id [description]
	 * @return [type]             [description]
	 */
	protected function attached_post_image( $image_name, $alt = '', $product_id ) {
		$result = $this->get_attachment_id_by_filename( $image_name );
		if ( $result ) {
			update_post_meta( $product_id, '_thumbnail_id', $result );
			if ( !empty( $alt ) ) {
				update_post_meta( $product_id, '_thumbnail_custom_all', $alt );
			}
			return true;
		}
		return false;
	}

	/**
	 * [get_attachment_id_by_filename description]
	 * @param  [type] $filename [description]
	 * @return [type]           [description]
	 */
	private function get_attachment_id_by_filename( $filename ) {
		global $wpdb;
		
		$result_id = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND guid LIKE '%$filename' ", ARRAY_A );
		if ( $result_id ) {
			return $result_id[0]['ID'];
		}
		return false;
	}

	/**
	 * [get_attachment_url_by_filename description]
	 * @param  [type] $filename [description]
	 * @return [type]           [description]
	 */
	private function get_attachment_url_by_filename( $filename ) {
		global $wpdb;
		
		$result_id = $wpdb->get_results( "SELECT guid FROM $wpdb->posts WHERE post_type = 'attachment' AND guid LIKE '%$filename' ", ARRAY_A );
		if ( $result_id ) {
			return $result_id[0]['guid'];
		}
		return false;
	}


	/**
	 * [set_product_type description]
	 * @param [type] $product_item [description]
	 */
	protected function set_product_type( $product_item, $product_parent_id ){
	// Check on child and parents.
		$set_terms_array = array();
		$many_explode = explode( ",", $product_item );

	// Each category
		foreach ($many_explode as $category_item ) {
		//Check on Tree cat
			$tree_cat = strripos( $category_item, '>' );
			if ( $tree_cat != false  ) {
			// Explode category item
				$tree_cat = explode( ">", $category_item );

				$check_parent = wp_set_object_terms( $product_parent_id, $tree_cat[0], 'product_cat' );
				if ( !is_wp_error( $check_parent ) ) {
				// Check exists parent and child 
					$parent_term = term_exists( $tree_cat[0], 'product_cat' );
					array_push( $set_terms_array, intval( $parent_term['term_id'] ) );

					$child_term = term_exists( $tree_cat[1], 'product_cat', $parent_term['term_id'] );
					if ( $child_term != false ) { // If child exists -> set his
						//wp_set_object_terms( $product_parent_id, $child_term['term_id'], 'product_cat' );
						array_push( $set_terms_array, intval( $child_term['term_id'] ) );
					}else {
					// Else create child term
						$new_term = wp_insert_term(
							$tree_cat[1],
							'product_cat',
							array(
								'parent' => $parent_term['term_id']
							)
						);
						// wp_set_object_terms( $product_parent_id, $new_term['term_id'], 'product_cat' );
						array_push( $set_terms_array, intval( $new_term['term_id'] ) );
					}
				}	
			}else { // If simple cat
				array_push( $set_terms_array, $category_item );
			}
		}
		wp_set_object_terms( $product_parent_id, $set_terms_array, 'product_cat' );
	}


	/**
	 * Delete BOM from UTF-8 file.
	 *
	 * @param string $fname
	 * @return void
	 */
	function stripBOM( $fname ) {
		$res = fopen($fname, 'rb');
		if ( false !== $res ) {
			$bytes = fread($res, 3);
			if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
				$this->log('Getting rid of byte order mark...' );
				fclose($res);

				$contents = file_get_contents($fname);
				if (false === $contents) {
					wp_die( 'Failed to get file contents.' );
				}
				$contents = substr($contents, 3);
				$success = file_put_contents($fname, $contents);
				if (false === $success) {
					wp_die( 'Failed to put file contents.' );
				}
			} else {
				fclose($res);
			}
		} else {
			$this->log( 'Failed to open file, aborting.' );
		}
	}

	/**
	 * Write log file
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function log( $data ) {
		if ( empty( $data ) )
			return 0;
	// Get default dir
		$upload_dir = wp_upload_dir();
	//Something to write to txt log
		$log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
				"Message: ". $data .PHP_EOL.
				"-------------------------".PHP_EOL;
	//Save string to log, use FILE_APPEND to append.
		$filename = $upload_dir['basedir'] . '/importCSV/log_' . date("j.n.Y").'.log';
		if ( file_exists( $filename ) ) {
			file_put_contents( $filename, $log, FILE_APPEND );
		}else{ 
			file_put_contents( $filename, $log );
		}
	}

}
new productImport;