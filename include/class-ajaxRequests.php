<?php 
/**
*
*
*
* 
*/

class ts_csvImport_AjaxRequests {
	
	function __construct() {
		self::addEvents();
	}

	public static function addEvents() {
		$events = array(
			'generate_lastImport_PricingDoc',
			'ts_pricesImport_action',
			'generate_withoutPrice_PricingDoc',
			'generate_chooseArtist_PricingDoc'

		);

		foreach ( $events as $event ) {
			add_action( 'wp_ajax_'.$event, array( __CLASS__, $event ) );
		}
	}

	public static function generate_chooseArtist_PricingDoc() {
		check_ajax_referer( 'importCsv_pricingtool', '_nonce' ); 
		if ( isset($_POST['artist']) ){
			$toolObject = new tsPricingTool();
			$product_ids = $toolObject->get_items_by_artist($_POST['artist']);
			if ( !empty( $product_ids ) ) {
				$items = $toolObject->create_items_array( $product_ids );
				$filepath = $toolObject->createfile_with( $items );
				if ( $filepath )
				    wp_send_json_success($filepath);
			}

		}
		wp_send_json_error();
	}

	public static function generate_withoutPrice_PricingDoc() {
		check_ajax_referer( 'importCsv_pricingtool', '_nonce' ); 

		$toolObject = new tsPricingTool();
		$items = $toolObject->get_items_withoutprice();

		if ( !empty( $items ) ) {
			$filepath = $toolObject->createfile_with($items);
			if ( $filepath )
				wp_send_json_success($filepath);
		}

		wp_send_json_error();
	}

	public static function generate_lastImport_PricingDoc() {
		check_ajax_referer( 'importCsv_pricingtool', '_nonce' ); 
		$toolObject = new tsPricingTool();
		$lastImport_ids = get_option('CJAffiliate_plugin_imported_ids');

		if ( empty( $lastImport_ids ) )
			wp_send_json_error('Last Import was not');

		$items = $toolObject->create_items_array( $lastImport_ids );
		if ( !empty( $items ) ) {
			$filepath = $toolObject->createfile_with($items);
			if ( $filepath )
				wp_send_json_success($filepath);
		}
		wp_send_json_error();
	}

	public static function ts_pricesImport_action(){
		check_ajax_referer( 'ts_import_pricingtool', '_wpnonce' ); 
		if ( empty($_FILES) )
			wp_send_json_error('Please attach import file');

		$importObject = new tsImportObject( $_FILES['importfile']['tmp_name'] );
		$importObject->import_price();

		wp_send_json_success();
	}


	





}	

new ts_csvImport_AjaxRequests;