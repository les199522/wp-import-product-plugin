<h1><?php echo get_admin_page_title(); ?></h1>
<div class="card">
	<h2>Import</h2>
	<p></p>
	<form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" class="form-csvImport js-csvImport" method="POST">
	<?php wp_nonce_field( 'importCsv_plugin' ); ?>
		<label>Attache file (.csv)
			<input type="file" class="input csv-input" name="importFile">
			<button type="sumbit" class="button btn-submit button-primary">Import All</button>
		<div class="cj-loader">
			<div class="cj-spinner">
			  <div class="rect1"></div>
			  <div class="rect2"></div>
			  <div class="rect3"></div>
			  <div class="rect4"></div>
			  <div class="rect5"></div>
			</div>
		</div>
		</label>
		<div class="rollback">
	        <button class="button btn-danger js-rollback-btn">Rollback Import</button>
	        <p class='js-rollbackComplete'></p>
	    </div>
	</form>
</div>
<h1>Pricing Tool</h1>
<div class="card pricing-tool">
	<?php wp_nonce_field('importCsv_pricingtool'); ?>
	<h2>Prices export</h2>
	<p></p>
	<div class="get-import-file">
		<div class="item-action">
			<button class="inline-btn button button-primary js-getlastimport">Get Last Import Tickets</button>
			<!-- <button class="inline-btn button button-primary js-getwithoutprices">Get All tickets without prices</button> -->
		</div>
		<div class="item-action">
			<span>Get all tickets choosen artist</span>
			<?php 
				$artist_terms = get_terms(array(
					'taxonomy' => 'artists',
					'hide_empty' => true
				));
			?>
			<?php if ( $artist_terms ): ?>
				<select class="select select2 js-choose-artist">
					<?php foreach ( $artist_terms as $term ): ?>
						<option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
					<?php endforeach ?>
				</select>
			<?php endif; ?>
			<button class="button button-primary js-getchooseartist" data-artist="" disabled>Get</button>
		</div>
		<p class="notice"></p>
		<a href="" class="button download-link" download>Download file</a>
	</div>
	<div class="">
		<h2>Prices Import</h2>
		<div>
			<form class="js-pricesimport-form" action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" enctype="multipart/form-data">
				<?php wp_nonce_field('ts_import_pricingtool'); ?>
				<input type="file" name="importfile">
				<input type="submit" value="Import">
			</form>
		</div>
	</div>
	<div class="cj-loader">
		<div class="cj-spinner">
			<div class="rect1"></div>
			<div class="rect2"></div>
			<div class="rect3"></div>
			<div class="rect4"></div>
			<div class="rect5"></div>
		</div>
	</div>

</div>